
# BibTeam

The source code is now available @ [https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/myGenomeBrowser](https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/myGenomeBrowser/)

# Publication
- <strong>myGenomeBrowser: building and sharing your own genome browser</strong>
Sébastien Carrere, Jérôme Gouzy
<i>_Bioinformatics_, Volume 33, Issue 8, 15 April 2017, Pages 1255–1257, [https://doi.org/10.1093/bioinformatics/btw800](https://doi.org/10.1093/bioinformatics/btw800)</i> 

# Production

- https://bbric-pipelines.toulouse.inra.fr/myGenomeBrowser

# Contact
Sebastien.Carrere@inrae.fr

